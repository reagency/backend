package core.contract.infracontract;

import java.util.List;

import core.domain.realestate.areaaggregate.City;
import core.domain.realestate.areaaggregate.District;

public interface ICityRepository extends IRepository<City> {
	public List<District> loadDistricts(City city);
}
