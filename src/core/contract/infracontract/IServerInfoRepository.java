package core.contract.infracontract;

import core.domain.kernel.ServerInfo;

public interface IServerInfoRepository extends IRepository<ServerInfo> {

}
