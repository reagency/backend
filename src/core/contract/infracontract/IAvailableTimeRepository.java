package core.contract.infracontract;

import java.util.List;

import core.domain.realestate.offeringaggregate.AvailableTime;
import core.domain.realestate.offeringaggregate.BookedTime;

public interface IAvailableTimeRepository extends IRepository<AvailableTime> {
	public List<BookedTime> loadRepresenters(AvailableTime availableTime);
}
