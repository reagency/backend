package core.contract.infracontract;

import java.util.List;

import core.domain.realestate.areaaggregate.Country;
import core.domain.realestate.areaaggregate.State;

public interface ICountryRepository extends IRepository<Country> {
	public List<State> loadStates(Country country);
}
