package core.contract.infracontract;

import java.util.List;

import core.domain.kernel.User;
import core.domain.realestate.offeringaggregate.BookedTime;
import core.domain.realestate.offeringaggregate.Offer;
import core.domain.realestate.offeringaggregate.SellingOffer;

public interface IUserRepository extends IRepository<User> {
	public List<BookedTime> loadBookedTimes(User user);
	public List<SellingOffer> loadFavorites(User user);
	public List<Offer> loadOffers(User user);
	public List<BookedTime> loadSellingOffers(User user);
}
