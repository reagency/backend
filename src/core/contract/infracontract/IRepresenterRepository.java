package core.contract.infracontract;

import java.util.List;

import core.domain.realestate.offeringaggregate.AvailableTime;
import core.domain.realestate.offeringaggregate.Representer;

public interface IRepresenterRepository extends IRepository<Representer> {
	public List<AvailableTime> loadRepresenters(Representer representer);
}
