package core.contract.infracontract;

import java.util.List;

import core.domain.realestate.agantaggregate.ExpertAgency;
import core.domain.realestate.estateaggregate.Estate;

public interface IExpertAgencyRepository extends IRepository<ExpertAgency> {
	public List<Estate> loadRepresenters(ExpertAgency expertAgency);
}
