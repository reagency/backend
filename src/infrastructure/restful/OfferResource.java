package infrastructure.restful;

import infrastructure.hib.DIConfiguration;
import infrastructure.hib.UnitOfWork;
import infrastructure.restful.viewmodels.OfferViewModel;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import core.applicationservice.MailSender;
import core.contract.infracontract.IUnitOfWork;
import core.domain.kernel.ServerInfo;
import core.domain.realestate.offeringaggregate.Offer;
import core.domain.realestate.offeringaggregate.OfferStatus;
import core.domain.realestate.offeringaggregate.SellingOffer;

@Path("offer")
public class OfferResource {
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("newOffer")
	public String addOffer(OfferViewModel offerViewModel) {
		boolean result = false;
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		SellingOffer sellingOffer = uow.getSellingOfferRepository().findById(
				offerViewModel.getOfferId());
		Offer offer = new Offer();
		offer.setName(offerViewModel.getName());
		offer.setDescription(offerViewModel.getOfferExtraInfo());
		offer.setEmail(offerViewModel.getEmail());
		offer.setFamily(offerViewModel.getFamily());
		offer.setPrice(offerViewModel.getOfferPrice());
		offer.setRegisterDate(new Date());
		sellingOffer.addOffer(offer);
		uow.getSellingOfferRepository().update(sellingOffer);
		uow.commit();
		context.close();
		MailSender mailSender = new MailSender();
		String message ="new offer is added into system for Selling Offer: "+ sellingOffer.getId() +"\nThe offered price is :" + offer.getPrice()+"$.";
		ServerInfo server =  uow.getServerInfoRepository().findById(1);
		String reciverEmail = server.getEmail();
		String subject = "new offer";
		mailSender.sendEmail(message, reciverEmail, subject);
		result = true;
		return (new Boolean(result).toString());
	}

	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("offers/{sellingOfferId}")
	public List<OfferViewModel> getOffers(
			@PathParam("sellingOfferId") int sellingOfferId) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		SellingOffer sellingOffer = uow.getSellingOfferRepository().findById(
				sellingOfferId);
		List<OfferViewModel> offersViewmodel = new ArrayList<>();
		if (sellingOffer != null) {
			sellingOffer.getOffers().size();

			List<Offer> offers = sellingOffer.getOffers();
			for (Offer offer : offers) {
				OfferViewModel model = new OfferViewModel();
				model.setOfferId(offer.getId());
				model.setEmail(offer.getEmail());
				model.setFamily(offer.getFamily());
				model.setName(offer.getName());
				model.setOfferExtraInfo(offer.getDescription());
				model.setOfferPrice(offer.getPrice());
				model.setPhone(offer.getPhone());
				offersViewmodel.add(model);
			}
		}

		uow.commit();
		context.close();
		return offersViewmodel;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("accept/{sellingOfferId}/{offerId}")
	public String acceptOffer(@PathParam("sellingOfferId") int sellingOfferId,
			@PathParam("offerId") int offerId) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);

		SellingOffer sellingOffer = uow.getSellingOfferRepository().findById(
				sellingOfferId);
		sellingOffer.getOffers().size();
		List<Offer> offers = sellingOffer.getOffers();
		MailSender mailSender = new MailSender();
		int soldPrice = 0;
		for (Offer offer : offers) {
			if (offer.getId() == offerId) {
				offer.setOfferStatus(OfferStatus.ACCEPTED);
				uow.getOfferRepository().update(offer);

				String name = "";
				if (offer.getCustomer() != null) {
					if (!offer.getCustomer().getName().equals("")) {
						name = offer.getCustomer().getName();
					} else if (!offer.getCustomer().getUsername().equals("")) {
						name = offer.getCustomer().getName();
					}
				} else if (!offer.getFamily().equals("")) {
					name = offer.getFamily();
				}
				String email = "";
				if (offer.getCustomer() != null) {
					if (!offer.getCustomer().getEmail().equals("")) {
						email = offer.getCustomer().getEmail();
					}
				} else if (!offer.getEmail().equals("")) {
					email = offer.getEmail();
				}
				String message = "Dear "
						+ name
						+ "\n\n"
						+ "your offer accepted. seller will call you as soon as posible.\n\nSincerely,\nTeam Zero";
				String subject = "Offer Accepted";
				String reciverEmail = email;

				mailSender.sendEmail(message, reciverEmail, subject);
				soldPrice = offer.getPrice();
			} else {
				offer.setOfferStatus(OfferStatus.DENIED);
				uow.getOfferRepository().update(offer);
				String name = "";
				if (offer.getCustomer() != null) {
					if (!offer.getCustomer().getName().equals("")) {
						name = offer.getCustomer().getName();
					} else if (!offer.getCustomer().getUsername().equals("")) {
						name = offer.getCustomer().getName();
					}
				} else if (!offer.getFamily().equals("")) {
					name = offer.getFamily();
				}
				String email = "";
				if (offer.getCustomer() != null) {
					if (!offer.getCustomer().getEmail().equals("")) {
						email = offer.getCustomer().getEmail();
					}
				} else if (!offer.getEmail().equals("")) {
					email = offer.getEmail();
				}
				String message = "Dear "
						+ name
						+ "\n\n"
						+ "your offer was rejected. Thank you for using our system.\n\nSincerely,\nTeam Zero";
				String subject = "Offer Result";
				String reciverEmail = email;

				mailSender.sendEmail(message, reciverEmail, subject);
			}
			uow.getOfferRepository().delete(offer.getId());
		}
		sellingOffer.setDateOfArchive(new Date());
		sellingOffer.setIsSold(true);
		sellingOffer.setSoldPrice(soldPrice);
		
		//sellingOffer.setIsArchived(true);
		uow.getSellingOfferRepository().update(sellingOffer);

		uow.commit();
		context.close();
		boolean result = true;
		return (new Boolean(result).toString());
	}

}
