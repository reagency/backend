package infrastructure.restful;

import infrastructure.hib.DIConfiguration;
import infrastructure.hib.UnitOfWork;
import infrastructure.restful.viewmodels.MailSenderViewModel;
import infrastructure.restful.viewmodels.ServerInfoViewModel;

import java.util.List;

import javax.ws.rs.Consumes;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;

import org.springframework.context.annotation.AnnotationConfigApplicationContext;

import core.contract.infracontract.IUnitOfWork;
import core.domain.kernel.ServerInfo;

@Path("server")
public class ServerResource {
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/sender")
	public MailSenderViewModel getMailSender() {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);

		List<ServerInfo> senders = uow.getServerInfoRepository().findAll();
		ServerInfo sender = null;
		if (senders.size() > 0) {
			sender = senders.get(0);
		}
		MailSenderViewModel model = new MailSenderViewModel();
		if (sender != null) {
			model.setEmail(sender.getEmail());
			model.setPassword(sender.getPassword());
		}
		uow.commit();
		context.close();
		return model;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("update/sender")
	public String updateSender(MailSenderViewModel sender) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		List<ServerInfo> senders = uow.getServerInfoRepository().findAll();
		ServerInfo senderObj = null;
		if (senders.size() > 0) {
			senderObj = senders.get(0);
		}
		boolean isDone = false;
		if (senderObj != null) {

			if (!senderObj.getEmail().equalsIgnoreCase(sender.getEmail())) {
				senderObj.setEmail(sender.getEmail());
			}
			if (!senderObj.getPassword().equalsIgnoreCase(sender.getPassword())) {
				senderObj.setPassword(sender.getPassword());
			}
			uow.getServerInfoRepository().update(senderObj);
			isDone = true;
		} else {
			ServerInfo newSender = new ServerInfo();
			newSender.setEmail(sender.getEmail());
			newSender.setPassword(sender.getPassword());
			newSender.setIsArchived(false);
			uow.getServerInfoRepository().save(newSender);
			isDone = true;
		}

		uow.commit();
		context.close();
		return (new Boolean(isDone).toString());
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/serverInfo")
	public ServerInfoViewModel getServerInfo() {

		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);

		List<ServerInfo> senders = uow.getServerInfoRepository().findAll();
		ServerInfo sender = null;
		if (senders.size() > 0) {
			sender = senders.get(0);
		}
		ServerInfoViewModel model = new ServerInfoViewModel();
		if (sender != null) {
			model.setOfferExpiry(sender.getOfferExpiry());
			model.setSellingExpiry(sender.getSellingExpiry());
		}
		uow.commit();
		context.close();
		return model;
	}

	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	@Consumes(MediaType.APPLICATION_JSON)
	@Path("update/offersetting")
	public String updateOffersSetting(ServerInfoViewModel info) {
		AnnotationConfigApplicationContext context = new AnnotationConfigApplicationContext(
				DIConfiguration.class);
		IUnitOfWork uow = context.getBean(UnitOfWork.class);
		List<ServerInfo> servers = uow.getServerInfoRepository().findAll();
		ServerInfo server = null;
		if(servers.size() > 0){
			server = servers.get(0);
		}
		
		boolean isDone = false;
		if (server != null) {

			if (server.getOfferExpiry() != info.getOfferExpiry()) {
				server.setOfferExpiry(info.getOfferExpiry());
			}
			if (server.getSellingExpiry() != info.getSellingExpiry()) {
				server.setSellingExpiry(info.getSellingExpiry());
			}
			uow.getServerInfoRepository().update(server);
			isDone = true;
		} else {
			ServerInfo newServer = new ServerInfo();
			newServer.setOfferExpiry(info.getOfferExpiry());
			newServer.setSellingExpiry(info.getSellingExpiry());
			uow.getServerInfoRepository().save(newServer);
			isDone = true;
		}

		uow.commit();
		context.close();
		return (new Boolean(isDone).toString());
	}


}
