package infrastructure.hib.repo.imp;

import java.util.List;

import org.hibernate.Hibernate;

import infrastructure.hib.Repository;
import core.contract.infracontract.ICountryRepository;
import core.domain.realestate.areaaggregate.City;
import core.domain.realestate.areaaggregate.Country;
import core.domain.realestate.areaaggregate.State;

public class CountryRepository extends Repository<Country> implements
		ICountryRepository {

	public CountryRepository(Class<?> type) {
		super(type);
	}
	public List<State> loadStates(Country country){
		int id = country.getStates().size();
		//Hibernate.initialize(country.getStates());
		return country.getStates();
	}

}
