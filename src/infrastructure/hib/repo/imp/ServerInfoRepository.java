package infrastructure.hib.repo.imp;

import infrastructure.hib.Repository;
import core.contract.infracontract.IServerInfoRepository;
import core.domain.kernel.ServerInfo;

public class ServerInfoRepository extends Repository<ServerInfo> implements IServerInfoRepository{

	public ServerInfoRepository(Class<?> type) {
		super(type);
	}

}
