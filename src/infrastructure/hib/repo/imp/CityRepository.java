package infrastructure.hib.repo.imp;

import infrastructure.hib.Repository;

import java.util.List;

import org.hibernate.Hibernate;

import core.contract.infracontract.ICityRepository;
import core.domain.realestate.areaaggregate.City;
import core.domain.realestate.areaaggregate.District;

public class CityRepository extends Repository<City> implements ICityRepository {

	public CityRepository(Class<City> type) {
		super(type);
		
	}
	public List<District> loadDistricts(City city){
		int id = city.getDistricts().size();
		//Hibernate.initialize(city.getDistricts());
		return city.getDistricts();
	}

}
