package infrastructure;
import javax.servlet.ServletContextEvent;
import javax.servlet.ServletContextListener;

import core.applicationservice.OffersOrganizerService;
import core.applicationservice.SellingOffersOrganizerService;

public class AppServletContextListener implements ServletContextListener {

	@Override
	public void contextDestroyed(ServletContextEvent arg0) {
		System.out.println("/////////////////////////////Application is stopped/////////////////////////////");
	}

	// Run this before web application is started
	@Override
	public void contextInitialized(ServletContextEvent arg0) {
		
		System.out.println("/////////////////////////////Application is started/////////////////////////////");
		Thread sellingThread = new Thread(new SellingOffersOrganizerService());
		sellingThread.start();
		Thread offerThread = new Thread(new OffersOrganizerService());
		offerThread.start();
	}
}
